#include <iostream>
using namespace std;

float min(float &a, float &b);

int main()
{
	//declaration
	float u_temp[7]={0};
	float u_inlet[3]={0};
	float input_temp;
	float input_inlet;
	float members[3][7]={5,3,1.5,0,-0.5,-1,-1.5,
						 3,2,1,0,-1,-2,-3,
						 1.5,1,0.5, 0, -1.5, -3, -5
						};
	float data=0;
	float total_data=0;
	
	//initialize
	cout<<"input temp: ";
	cin>>input_temp;
	cout<<"input inlet: ";
	cin>>input_inlet;
	//fuzzification Tank Temperature
	if( input_temp <= 30 && input_temp >= 20)
	{
		u_temp[0]= 3- 0.1*input_temp;
		u_temp[1]=0.1*input_temp -2;
	}else if( input_temp <= 40 )
	{
		u_temp[1]= 4-0.1*input_temp;
		u_temp[2]= 0.1*input_temp - 3;
	}else if( input_temp <= 50 )
	{
		u_temp[2]=5-0.1*input_temp;
		u_temp[3]=0.1*input_temp - 4;
	}else if( input_temp <= 60 )
	{
		u_temp[3]=6-0.1*input_temp;
		u_temp[4]=0.1*input_temp - 5;
	}else if( input_temp <= 70 )
	{
		u_temp[4]=7-0.1*input_temp;
		u_temp[5]=0.1*input_temp - 6;
	}else if( input_temp <= 80 )
	{
		u_temp[5]=8-0.1*input_temp;
		if( input_temp <= 75)
		{
			u_temp[6]= 0.2*input_temp - 14;
		}else
		{
			u_temp[6]=1;
		}
	}else
	{
		u_temp[6]= 1;
	}
	//fuzzification Inlet Temperature
	if(input_inlet<=5)
	{
		u_inlet[0]=1;
	}else if( input_inlet<=15 )
	{
		u_inlet[0]=1.5 -0.1*input_inlet;
		u_inlet[1]=0.1*input_inlet-0.5;
	}else if( input_inlet<= 25 )
	{
		u_inlet[1]=2.5-0.1*input_inlet;
		u_inlet[2]=0.1*input_inlet-1.5;
	}else
	{
		u_inlet[2]=1;
	}
	//fuzzy interference system
	for(int i=0 ;i<3; i++)
	{
		for(int j=0; j<7; j++)
		{
			total_data+=min(u_inlet[i],u_temp[j])*members[i][j];
			data+=min(u_inlet[i], u_temp[j]);
		}
	}
	//defuzzification
	cout<<"hasil: "<<total_data/data<<endl;
	cout<<"-----------------------------------"<<endl;
	return 0;
}

float min(float &a, float &b)
{
	return (a<b)?a:b;
}